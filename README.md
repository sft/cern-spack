# [Spack](https://github.com/LLNL/spack) package repo for CERN software packaging

This holds a set of Spack packages for common CERN software, as a private
repository.  It relies on two others repositories:

- [HEP](https://github.com/HEP-SF/hep-spack) and the hep-spack Spack packages
- Spack and the builtin Spack packages

Some of them are overridden by this repo. The aim of this repo is to provide
the software used at Cern processes, with special builds or being tested to
send it to one of the previously mentioned.

## Getting started

Initial setup like:

```bash
cd /path/to/big/disk
git clone https://github.com/LLNL/spack.git
cd spack/var/spack/repos
git clone https://github.com/HEP-SF/hep-spack.git
cd -
./spack/bin/spack compiler add /usr/bin/gcc
./spack/bin/spack repo add spack/var/spack/repos/hep-spack
cd spack/var/spack/repos
git clone https://gitlab.cern.ch/sft/cern-spack.git
cd -
./spack/bin/spack repo add spack/var/spack/repos/cern-spack
```

To not have to type a full path to `spack` and to gain some other shell-level features do

```bash
$ source spack/share/spack/setup-env.sh
```
