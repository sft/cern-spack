#!/usr/bin/python

import sys
import os
import subprocess

def executeShellCommand(cmd, compiler):
    print "Running Spack..."
    print "Current configuration:"
    sys.stdout.flush()
    if compiler != 'native':
        subprocess.call(["spack", "compiler", "info", compiler])
    #print "\tLD_LIBRARY_PATH: " + os.environ['LD_LIBRARY_PATH'] + "\n"
    print "Executing command: " + cmd
    sys.stdout.flush()
    subprocess.call(cmd.split())

# Original spack configuration file
#spack_vfile     =  os.environ['SPACK_ROOT']+"/etc/spack/defaults/packages.yaml"

# Heptools file with ours versions
#heptools_vfile   = os.environ['SPACK_ROOT']+"/heptoolsSpack.yaml"

# Read yaml structure
#spackconf = yaml.load(open(spack_vfile, 'r'))
#hepconf = yaml.load(open(heptools_vfile, 'r'))

# Append package version to spack yaml
#spackconf['packages'].update(hepconf)

# Write the spack configuration file with new structure in a user file
#user_vfile = os.environ['HOME']+"/.spack/packages.yaml"
#with open(user_vfile, 'w') as yaml_file:
#    yaml_file.write(yaml.dump(spackconf))

# Check number of arguments
if(len(sys.argv) < 3):
    sys.exit("Expected three arguments. Execute: \n \
        python runSpack.py Target CompilerName [CompilerVersion]  \
    ")

# Read arguments
target = sys.argv[1].split(",")
compiler = sys.argv[2]

if(len(sys.argv) >= 4):
    version = sys.argv[3]

verbose = False

extra_opts = None
deps_opts = None
cdash = None

if len(sys.argv) >= 5:
    verbose = sys.argv[4]
if len(sys.argv) >= 6:
    cdash = sys.argv[5]
if len(sys.argv) >= 7:
    extra_opts = sys.argv[6]
if len(sys.argv) >= 8:
    deps_opts = sys.argv[7]

spackCMD = "spack "
if (verbose == "true"):
    spackCMD += "-v "

spackCMD += "install "

if extra_opts:
    spackCMD += extra_opts

if (cdash == "true"):
    logfile = "%s-cdash" % target
    spackCMD += "--log-format=junit --log-file %s" % logfile

spackReindexCMD = ["spack", "reindex"]

# First call reindex command to index and find correctly packages already installed
subprocess.call(spackReindexCMD)

# Build every package defined in the heptools yaml file
if target == 'all':
    for pkg in hepconf.keys():
        if compiler == 'native':
            cmd = spackCMD + " " + pkg
        else:
            cmd = spackCMD + " " + pkg + " %"+compiler+"@"+version
        executeShellCommand(cmd, compiler)
else:
    for pkg in target:
        #call(spackCMD + [pkg, "%"+compiler+"@"+version])
        if compiler == 'native':
            cmd = spackCMD + " " + pkg
        else:
            cmd = spackCMD + " " + pkg + " %"+compiler+"@"+version
        if deps_opts:
            cmd += " " + deps_opts
        executeShellCommand(cmd, compiler)
        if cdash == "true":
            command = "curl --upload-file %s http://cdash.cern.ch/submit.php?project=Spack" % logfile
            subprocess.call(command.split(" "))
