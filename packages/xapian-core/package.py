##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *


class XapianCore(Package):
    """Xapian is a highly adaptable toolkit which allows developers to easily
       add advanced indexing and search facilities to their own applications.
       It supports the Probabilistic Information Retrieval model and also
       supports a rich set of boolean query operators.."""

    homepage = "http://www.xapian.org"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "xapian-core-1.2.21.tar.gz"

    version('1.2.21', 'bd01dccacb6314e853cf80fb29ac15f7')

    def install(self, spec, prefix):
        configure('--prefix={0}'.format(prefix))
        make()
        cmake('-E', 'copy', '{0}/include/xapian.h'.format(prefix),
            '{0}/include/xapian/.'.format(prefix))
        make('install')
