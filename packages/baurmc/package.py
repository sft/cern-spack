##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Baurmc(Package):
    """Baur's Monte Carlo package for simulating W+ gamma production at hadron
    colliders provides full electroweak calculation upto O(alpha) and contains
    higher order QCD corrections as well."""

    homepage = "https://twiki.cern.ch/twiki/bin/view/Sandbox/BaurMCInteface"
    url      = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/baurmc-1.0-src.tgz"

    version('1.0', '7167ca26a0498df0475adfe20ca72fb6')

    def install(self, spec, prefix):
        options = [
            '--userfflags=-fno-automatic',
             '--enable-shared'
        ]
        configure(*options)
        make()
        make('install')
