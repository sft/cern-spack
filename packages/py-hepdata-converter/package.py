##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyHepdataConverter(PythonPackage):
    """Library providing means of conversion between oldhepdata format to new
    one, and new one to csv / yoda / root etc."""

    homepage = "https://github.com/HEPData/hepdata-converter/"
    url      = "https://pypi.io/packages/source/h/hepdata-converter/hepdata-converter-0.1.29.tar.gz"

    version('0.1.29', 'b3026524a046d57139eb379fdbd781d5')
    version('0.1.28', '5070603e27d1ac14451916a751f4806e')
    version('0.1.27', '2e67ca8a5fa4149cdc2a0e204c900553')
    version('0.1.26', 'deca3a83365b83177922638b2ce0f692')
    version('0.1.25', '8945d78327b1276f2a7c681a5180e440')
    #version('0.1.24', 'ba0a90180278e16745e6d808879bbe85',  # not in pypi.org
    #        url = "https://github.com/HEPData/hepdata-converter/archive/0.1.24.tar.gz")
    version('0.1.23', 'a1f931c322022d04b1171210b07a0768')
    version('0.1.22', 'ac2fd542f263e16db13575a4dcb97c2a')

    depends_on('py-setuptools', type='build')
    depends_on('py-matplotlib', type=('build', 'run'))
    depends_on('py-hepdata-validator', type=('build', 'run'))
