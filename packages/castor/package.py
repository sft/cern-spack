##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *

class Castor(Package):
    """The CERN Advanced STORage manager (CASTOR) is a hierarchical storage
    (i.e. has disk and tape) management system which was developed at CERN for
    archiving physics data (with very large data volumes, see the plot on the
    right"""

    homepage = "http://castor.web.cern.ch/"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "CASTOR-2.1.13-6-x86_64-slc6.tar.gz"

    version('2.1.13-6-x86_64-slc6' , '666969e3ca605763905c85d85241d09a')

    depends_on("cmake", type='build')
    depends_on("oracle", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '{0}'.format(source_directory),
            '{0}'.format(prefix))
        env['CASTOR_HOME'] = prefix

    def url_for_version(self, version):
        return "{0}CASTOR-{1}.tar.gz".format(self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        castorhome = self.prefix
        spack_env.set('CASTOR_HOME', castorhome)
