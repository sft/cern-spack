##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Agile(Package):
    """AGILe is A Generator Interface Library (& executable), i.e. a uniform
    object oriented C++ interface for a variety of Fortran-based Monte Carlo
    event generators.."""

    homepage = "https://agile.hepforge.org/"
    url      = "http://www.hepforge.org/archive/agile/AGILe-1.4.1.tar.gz"

    version('1.4.1', '2bb0bf38bb3a1404dfb5caeea719b84a')
    version('1.4.0', 'f7a6a613468af3e40fd23cff2473e498')
    version('1.3.0', '7a97a66267181a0e5f081a461fc6d59a')

    depends_on('hepmc')
    depends_on('boost')
    depends_on('python')
    depends_on('swig')
    depends_on('py-future')

    def install(self, spec, prefix):
        options = ['--prefix=%s' % prefix,
                   '--with-hepmc=%s' % spec['hepmc'].prefix,
                   '--with-boost=%s' % spec['boost'].prefix,
                   'CFLAGS=-g0 -O2',
                   'CXXFLAGS=-g0 -O2',
                   'FFLAGS=-g0 -O2']
        configure(*options)
        make()
        make("install")
