##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform

class Oracle(Package):
    """Object-relational database management system produced and marketed by
    Oracle Corporation."""

    homepage = "http://www.oracle.com"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "oracle-11.2.0.3.0-linux64.tar.gz"

    plt = platform.machine() + '-' + platform.system()

    if plt == 'x86_64-Linux':
        version('11.2.0.3.0-linux64', '5cf4642d06162b8095c7391aabccbdf4',
            url=list_url+'oracle-11.2.0.3.0-linux64.tar.gz')
    elif plt == 'x86_64-Darwin':
        version('11.2.0.3.0-macosx64', 'e4c8c48cef64a99b99235c5ca3240ba2',
            url=list_url+'oracle-11.2.0.3.0-macosx64.tar.gz')
    elif plt == 'i636-Linux':
        version('11.2.0.3.0-linux32', '2985ebb49ac19df0f2a7d02406f0d39e',
            url=list_url+'oracle-11.2.0.3.0-linux32.tar.gz')
    else:
        version('11.2.0.3.0-macosx32', '8821cd209cea48f50b9c54c146d4d37e',
            url=list_url+'oracle-11.2.0.3.0-macosx32.tar.gz')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path
        cmake('-E', 'copy_directory', '{0}'.format(source_directory),
            '{0}/.'.format(prefix))

        env['ORACLE_HOME'] = prefix

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        oraclehome = self.prefix
        spack_env.set('ORACLE_HOME', oraclehome)
