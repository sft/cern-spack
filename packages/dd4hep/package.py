##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform


class Dd4hep(CMakePackage):
    """Detector Description for HEP"""

    homepage = "http://aidasoft.web.cern.ch/DD4hep"
    url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/DD4hep-00-17.tar.gz "

    version('00-17', 'e4e107268ff6cbbe8a75ea34dc6194ec')

    depends_on('cmake@3.6:', type='build')

    depends_on("ROOT")
    depends_on("xerces-c")
    depends_on("geant4")
    depends_on("boost")

    def build_type(self):
        spec = self.spec
        if '+debug' in spec:
            return 'Debug'
        else:
            return 'Release'

    def cmake_args(self):
        spec = self.spec
        options = [
            '-DCMAKE_BUILD_TYPE:STRING=Release',
            '-DBOOST_ROOT=%s' % spec['boost'].prefix,
            '-DDD4HEP_USE_XERCESC=ON',
            '-DXERCESC_ROOT_DIR= %s' % spec['xerces-c'].prefix,
            '-DROOTSYS= %s' % spec['ROOT'].prefix,
            '-DDD4HEP_USE_GEANT4=ON'
        ]

        # Boost configuration
        options.append('-DBoost_NO_BOOST_CMAKE=ON')
        options.append('-DBoost_NO_SYSTEM_PATHS=ON')

        plt = platform.system().lower()
        if(plt and 'clang' in spec):
            options.append('-DBoost_COMPILER=-xgcc42')

        if '+cxx11' in spec:
            options.append('-DDD4HEP_USE_CXX11=ON')
        if '+cxx14' or '+cxx1y' in spec:
            options.append('-DDD4HEP_USE_CXX14=ON')
