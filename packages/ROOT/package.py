##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import os
import platform
import sys
import re

def getPlatform():
    arch = platform.machine()

    system = platform.system()

    #---Determine the OS and version--------------------------------------
    if system == 'Darwin' :
       osvers = 'mac' + string.join(platform.mac_ver()[0].split('.')[:2],'')
    elif system == 'Linux' :
       dist = platform.linux_distribution()
       if re.search('SLC', dist[0]):
          osvers = 'slc' + dist[1].split('.')[0]
       elif re.search('CentOS', dist[0]):
          osvers = 'centos' + dist[1].split('.')[0]
       elif re.search('Ubuntu', dist[0]):
          osvers = 'ubuntu' + dist[1].split('.')[0]
       elif re.search('Fedora', dist[0]):
          osvers = 'fedora' + dist[1].split('.')[0]
       else:
          distribution = platform.linux_distribution()
          osvers = 'linux' + string.join(distribution[1].split('.')[:2],'')
    elif system == 'Windows':
       osvers = win + platform.win32_ver()[0]
    else:
       osvers = 'unk-os'
    return '%s-%s' %(arch, osvers)

plt = getPlatform()

class Root(Package):
    """A modular scientific software framework. It provides all the
    functionalities needed to deal with big data processing, statistical
    analysis, visualisation and storage. It is mainly written in C++ but
    integrated with other languages such as Python and R."""

    homepage = "http://root.cern.ch"

    # Git repository
    version('HEAD', git='http://root.cern.ch/git/root.git')

    variant('graphviz', default=False, description='Enable graphviz support')

    # Dependencies
    depends_on('cmake', type='build')
    depends_on("pcre")
    depends_on('fftw')
    depends_on('graphviz', when='+graphviz')
    depends_on('python')
    depends_on('gsl')
    depends_on('giflib')
    depends_on("libxml2")
    depends_on("jpeg")
    depends_on('mysql')
    depends_on('xrootd')
    depends_on('r')
    depends_on('py-numpy')

    depends_on('vc', when='@6.0:')
    depends_on('vc', when='@v6:')

    if not 'mac' in plt:
        depends_on('davix')

    if 'slc' in plt or 'centos' in plt:
        depends_on('oracle')
        depends_on('qt')

    if 'x86_64-slc' in plt:
        depends_on('castor')
        depends_on('dcap')
        depends_on('gfal')
        depends_on('srm_ifce')

    if 'mac' in plt and not ('mac109' in plt or 'mac1010' in plt):
        depends_on('qt')

    # R-packages dependencies
    depends_on('r-rcpp')
    depends_on('r-rinside')
    # depends_on('r-deoptim')
    # depends_on('r-base64enc')
    # depends_on('r-evaluate')
    # depends_on('r-c50')
    # depends_on('r-xgboost')
    # depends_on('r-rsnns')
    # depends_on('r-e1071')
    # depends_on('r-uuid')
    # depends_on('r-jsonlite')
    # depends_on('r-digest')
    # depends_on('r-ggplot2')
    # depends_on('r-plyr')
    # depends_on('r-reshape2')
    # depends_on('r-dplyr')
    # depends_on('r-tidyr')
    # depends_on('r-caret')
    # depends_on('r-randomforest')
    # depends_on('r-datatable')
    # depends_on('r-quantmod')
    # depends_on('r-shiny')
    # depends_on('r-rmarkdown')
    # depends_on('r-glmnet')
    # depends_on('r-zoo')
    # depends_on('r-rbokeh')
    # depends_on('r-rzmq')
    # depends_on('r-repr')
    # depends_on('r-irkernel')
    # depends_on('r-irdisplay')

    def install(self, spec, prefix):

        build_directory = join_path(self.stage.path, 'spack-build')
        source_directory = self.stage.source_path
        cmake_args = [source_directory]

        if '+debug' in spec:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Debug')
        else:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Release')

        cmake_args.extend([
            '-DCMAKE_INSTALL_PREFIX=%s' % prefix,
            '-Dpython:BOOL=ON',
            '-Dbuiltin_pcre:BOOL=ON',
            '-Dcintex:BOOL=ON',
            '-Dexceptions:BOOL=ON',
            '-Dexplicitlink:BOOL=ON',
            '-Dfftw3:BOOL=ON',
            '-Dgdml:BOOL=ON',
            '-Dgsl_shared:BOOL=ON',
            '-Dhttp:BOOL=ON',
            '-Dkrb5:BOOL=ON',
            '-Dgenvector:BOOL=ON',
            '-Dldap:BOOL=OFF',
            '-Dmathmore:BOOL=ON',
            '-Dmemstat:BOOL=ON',
            '-Dminuit2:BOOL=ON',
            '-Dmysql:BOOL=ON',
            '-Dodbc:BOOL=ON',
            '-Dopengl:BOOL=ON',
            '-Dpgsql:BOOL=OFF',
            '-Dqtgsi:BOOL=ON',
            '-Dreflex:BOOL=ON',
            '-Dr:BOOL=ON',
            '-Droofit:BOOL=ON',
            '-Drfio:BOOL=ON',
            '-Dssl:BOOL=ON',
            '-Dtable:BOOL=ON',
            '-Dunuran:BOOL=ON',
            '-Dxft:BOOL=ON',
            '-Dxml:BOOL=ON',
            '-Dxrootd:BOOL=ON',
            '-DCINTMAXSTRUCT=36000',
            '-DCINTMAXTYPEDEF=36000',
            '-DCINTLONGLINE=4096',
            '-Dcxx14=on',
            '-Dc++14=on'])

        if '+python@3' in spec:
            cmake_args.append('-Dpython3=ON')

        if not 'mac' in plt:
            cmake_args.append('-Ddavix:Bool=ON')

        if 'x86_64-slc' in plt:
            cmake_args.append('-Dcastor=ON')
            cmake_args.append('-Ddcache=ON')
            cmake_args.append('-Dgfal=ON')
            cmake_args.append('-DGFAL_DIR=%s' % spec['gfal'].prefix)
            cmake_args.append('-DSRM_IFCE_DIR=%s' % spec['srm_ifce'].prefix)

        if 'slc' in plt or 'centos' in plt:
            cmake_args.append('-Doracle=ON')
            cmake_args.append('-DORACLE_HOME=%s' % spec['oracle'].prefix)
            cmake_args.append('-Dqt=ON')

        if 'mac' in plt and not ('mac109' in plt or 'mac1010' in plt):
            cmake_args.append('-Dqt=ON')

        if '@6:' in spec and not 'mac' in plt:
            cmake_args.append('-Dvc=ON')


        cmake_args.extend(std_cmake_args)

        with working_dir(build_directory, create=True):
            cmake(*cmake_args)
            make()
            make("install")

    def setup_dependent_environment(self, spack_env, run_env, dspec):
        spack_env.set('ROOTSYS', self.prefix)
        spack_env.set('ROOT_VERSION', 'v6')
        spack_env.prepend_path('PYTHONPATH', self.prefix.lib)
