##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Gsoap(AutotoolsPackage):
    """The gSOAP tools provide an automated SOAP and XML data binding for C and
    C++ based on compiler technologies."""

    homepage = "https://genivia.com/dev.html"
    url      = "https://sourceforge.net/projects/gsoap2/files/gsoap-2.8/gsoap_2.8.44.zip"


    version('2.8.44', '9117fbd45319cd1e64bd024f383fabbe')

    variant('zlib', default=False)

    depends_on('openssl')
    depends_on('zlib', when="+zlib")

    parallel = False

    def configure_args(self):
        spec = self.spec
        args = [
             '--with-openssl=%s' % spec['openssl'].prefix,
        ]

        if "+zlib" in spec:
            args.append('--with-zlib=%s' % spec['zlib'].prefix)

        return args
