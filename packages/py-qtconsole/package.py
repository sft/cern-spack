##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyQtconsole(PythonPackage):
    """Jupyter Qt Console"""

    homepage = "https://github.com/jupyter/qtconsole/"
    url      = "https://github.com/jupyter/qtconsole/archive/4.2.1.tar.gz"

    version('4.2.1', '97a3e29cd69bc0270411b313c907c173')
    version('4.2.0', '63c8f88830e41007dc1b807f02a250f9')
    version('4.1.1', '09edb4932e59a07f26ff1fd492c9f740')
    version('4.1.0', '194024f38e05f1a624d51f68ee1879db')
    version('4.0.1', '5552d5d3342570a816a443ab7e476c54')

    depends_on('py-setuptools', type='build')
    depends_on('py-traitlets', type=('build', 'run'))
    depends_on('py-ipython-genutils', type=('build', 'run'))
    depends_on('py-jupyter-core', type=('build', 'run'))
    depends_on('py-jupyter-client@4.1:', type=('build', 'run'))
    depends_on('py-pygments', type=('build', 'run'))
    depends_on('py-ipykernel@4.1:', type=('build', 'run'))
