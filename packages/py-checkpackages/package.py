##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyCheckpackages(PythonPackage):
    """Package to test python-packages"""

    homepage = "https://github.com/uqfoundation/dill"
    url      = "https://pypi.org/packages/source/d/dill/dill-0.2.6.zip"

    version('0.2.6', 'f8b98b15223d23431024349f2102b4f9')

    depends_on('py-4suite-xml')
    depends_on('py-dill')
    #depends_on('py-elasticsearch')
    depends_on('py-hepdata-validator')
    depends_on('py-hepdata-converter')
    depends_on('py-jpype')
    depends_on('py-lxml')
    depends_on('py-pathos')
    depends_on('py-ppft')
    depends_on('py-pyxml')
    depends_on('py-xrootd-python')
    depends_on('py-stomp-py')
    depends_on('py-urllib3')
    depends_on('py-requests')
    depends_on('py-subprocess32')
    depends_on('py-pyzmq')
    depends_on('py-py4j')
    depends_on('py-processing')
    depends_on('py-pox')
    depends_on('py-multiprocess')
    depends_on('py-multiprocessing')
    depends_on('py-messaging')
    depends_on('py-joblib')
    depends_on('py-pytimber')
    depends_on('py-pygsi')
    depends_on('py-future')
    depends_on('py-distribute')
    depends_on('py-cx-oracle')
    depends_on('py-couchdb')
