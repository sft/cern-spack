##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *
import platform


class Coin(AutotoolsPackage):
    """Coin3D is a high-level, retained-mode toolkit for effective 3D graphics
    development. It is API compatible with Open Inventor 2.1."""

    homepage = "https://bitbucket.org/Coin3D/coin/wiki/Home"
    url      = "https://bitbucket.org/Coin3D/coin/downloads/Coin-3.1.3.tar.gz"

    version('3.1.3', '1538682f8d92cdf03e845c786879fbea')

    variant('debug', default=False, description='Build debug version')
    variant('symbols', default=False,
            description='Include symbol debug information')

    patch('freetype.patch')

    def configure_args(self):
        args = []
        spec = self.spec

        # By default: enable-debug
        if '+debug' in spec:
            args.append('--enable-debug')
        else:
            args.append('--disable-debug')

        # By default: enable-symbols
        if '+symbols' in spec:
            args.append('--enable-symbols')
        else:
            args.append('--disable-symbols')

        if 'Darwin' in platform.system():
            args.append('--without-framework')

        return args
