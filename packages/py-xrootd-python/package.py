##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *
import platform
import os


class PyXrootdPython(PythonPackage):
    """Python bindings for XRootD"""

    homepage = "https://github.com/xrootd/xrootd-python/"
    url      = "https://github.com/xrootd/xrootd-python/archive/v0.3.0.tar.gz"

    version('0.3.0', '022732104b779d34cbeadb437b5c6ecf')
    version('0.2.0', '3973f89be065d8d8de38fd9d33165e97')
    version('0.1.3', '05cdac03e0c471e7cfa5806cdb852339')

    depends_on('xrootd')

    def setup_environment(self, spack_env, run_env):
        spec = self.spec
        plt = platform.architecture()
        if '64bit' in plt:
            os.environ["XRD_LIBDIR"] = join_path(spec['xrootd'].prefix, 'lib64')
        else:
            os.environ["XRD_INCDIR"] = join_path(spec['xrootd'].prefix, 'lib')

        os.environ["XRD_INCDIR"] = join_path(spec['xrootd'].prefix, 'include/xrootd')
