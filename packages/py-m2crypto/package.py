
##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyM2crypto(PythonPackage):
    """M2Crypto is a crypto and SSL toolkit for Python."""

    homepage = "https://gitlab.com/m2crypto/m2crypto"
    url      = "https://pypi.io/packages/source/m/m2crypto/M2Crypto-0.26.0.tar.gz"

    version('0.26.0', '9f02f0b88fbe225cc6ea8680945cafa0')

    variant('openssl', default=False)
    variant('swig', default=False)

    depends_on('py-setuptools', type='build')
    depends_on('py-typing', type=('build', 'run'))
    #depends_on('py-unittest2', type=('build', 'run'), when="^python@:2.6")

    depends_on('openssl@:1.0.1', type=('build', 'run'), when="+openssl")
    depends_on('swig@2.0.4:', type=('build', 'run'), when="+swig")

    def build_args(self, spec, prefix):
        args = []
        if '+openssl' in self.spec:
            args.append('--openssl=%s' % self.spec['openssl'].prefix)
        return args
