##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform
import os

class FrontierClient(Package):
    """Client and server software that provides http read access to SQL-based
        databases"""

    homepage = "https://cdcvs.fnal.gov/redmine/projects/frontier/"
    url = "frontier.cern.ch/dist/frontier_client__2.8.20__src.tar.gz"

    version('2.8.20', 'e2ea893b02eab539a0cf6a3812f4937c')
    version('2.8.19', '51acddfd70c1bd77c13d2ef03538638d')

    depends_on('expat', when='@2.8.6:')
    depends_on('pacparser', when='@2.8.6:')

    depends_on("cmake", type='build')

    plt = platform.machine().lower()
    if 'darwin' in plt:
        depends_on('openssl')

    def install(self, spec, prefix):
        source_directory = join_path(self.stage.source_path, 'dist')

        make_args = ['dist']
        if 'expat' in spec:
            expat_home = spec['expat'].prefix
            os.environ["LD_LIBRARY_PATH"] = os.environ["LD_LIBRARY_PATH"] + ":%s/lib" % expat_home
            make_args.append('EXPAT_DIR=%s' % spec['expat'].prefix)

        if 'pacparser' in spec:
            make_args.append('PACPARSER_DIR=%s' % spec['pacparser'].prefix)

        plt = platform.machine().lower()
        if 'darwin' in plt:
            make_args.append('OPENSSL_DIR=%s' % spec['openssl'].prefix)

        make(*make_args)
        cmake('-E', 'copy_directory', '{0}'.format(source_directory), '{0}'.format(prefix))
        make('install')

    def url_for_version(self, version):
        return "frontier.cern.ch/dist/frontier_client__%s__src.tar.gz" % version
