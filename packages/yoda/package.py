##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Yoda(AutotoolsPackage):
    """YODA is a small set of data analysis (specifically histogramming)
    classes being developed by MCnet members as a lightweight common system
    for MC event generator validation analyses, particularly as the core
    histogramming system in Rivet."""

    homepage = "https://yoda.hepforge.org/"
    url      = "http://www.hepforge.org/archive/yoda/YODA-1.6.6.tar.gz"

    version('1.6.6', '4fa8484a043cfda52c049352a5881a88')
    version('1.6.5', 'd1b4bbd41c564f99a50b5b647823b4e0')
    version('1.6.4', '0c9317f212f783aa480c3199d063c7cc')

    depends_on('python')
    depends_on('py-cython')
    depends_on('py-future')
    depends_on('boost', when="@:1.6.0")

    def configure_args(self):
        args = []
        return args
