##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform
import re


class Gridsite(Package):
    """Gridsite binary package, Grid externals"""

    homepage = "????"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "gridsite-1.7.25-1.emi2-x86_64-slc6.tar.gz"

    v="1.7.25-1.emi2"
    plt = platform.system()

    if 'SLC6' in plt and '6.8' in plt:
        version(v+'-x86_64-slc6', '7450462209c8344a00b5e61f93fbcff2')
    elif 'CentOS' in plt:
        version(v+'-x86_64-centos7', '7450462209c8344a00b5e61f93fbcff2')
    else:
        version(v+'-x86_64-slc5', '4bc79303d7bda6822dd5c9476785e931')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '%s' % source_directory,
            '%s' % prefix)
        env['GRIDSITE_HOME'] = prefix

    def url_for_version(self, version):
        return "%sgridsite-%s.tar.gz" % (self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        spack_env.set('GRIDSITE_HOME', self.prefix)
