##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform
import re


class Fts(Package):
    """Fts binary package, Grid externals"""

    homepage = "????"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "FTS-2.2.8emi2-x86_64-slc6.tar.gz"

    v="2.2.8emi2"
    plt = platform.system()

    if 'SLC6' in plt and '6.8' in plt:
        version(v+'-x86_64-slc6', 'a9a3dcde04c5547779cbc7939fd63e17')
    elif 'CentOS' in plt:
        version(v+'-x86_64-centos7', 'a9a3dcde04c5547779cbc7939fd63e17')
    else:
        version(v+'-x86_64-slc5', '962f411320d1515e1adf97570b56c223')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '%s' % source_directory,
            '%s' % prefix)
        env['FTS_HOME'] = prefix

    def url_for_version(self, version):
        return "%sFTS-%s.tar.gz" % (self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        spack_env.set('FTS_HOME', self.prefix)
