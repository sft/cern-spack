##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyRequests(PythonPackage):
    """Python HTTP Requests for Humans"""

    homepage = "http://python-requests.org"
    url      = "https://github.com/kennethreitz/requests/archive/v2.13.0.tar.gz"

    version('2.13.0', '94ad79c2e57917aca999308b1fb4cbb4')
    version('2.12.5', '622e30de076bdca7900e612c78489882')
    version('2.12.4', '0b7637c9a9e8b02d2258c91841d049f2')
    version('2.12.3', 'bae63b3d54e548ccd97b86573a43f1aa')
    version('2.12.2', 'a25eb0b8cc6f3fda5dee79264d18de39')
    version('2.12.1', '555daab337d04bd24f725eebccb8473d')
    version('2.12.0', 'e8bcb0398b9161ebe78c130b1cbbc3e8')
    version('2.11.1', '3007b144a7ce83ee16262b79e3acfa1b')
    version('2.11.0', '09de9d563cd43ee28a3c55fc308c71f0')
    version('2.10.0', '03000b031eabe33c579248e1c78aea60')

    depends_on('py-setuptools', type='build')
