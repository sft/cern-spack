##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import os
import platform
import sys
import re

class Davix(Package):
    """The davix project aims to make file management over HTTP-based protocols
    simple. The focus is on high-performance remote I/O and data management of
    large collections of files. Currently, there is support for the WebDav
    (link is external), Amazon S3 (link is external), Microsoft Azure (link is
    external), and HTTP (link is external) protocols."""

    homepage = "http://dmc.web.cern.ch/projects/davix/home"

    # Git repository
    version('0.6.2', git='https://github.com/cern-it-sdc-id/davix',
        tag='R_0_6_2')
    version('0.6.3', git='https://github.com/cern-it-sdc-id/davix',
        tag='R_0_6_3')
    version('0.6.4', git='https://github.com/cern-it-sdc-id/davix',
        tag='R_0_6_4')

    # Dependencies
    depends_on('boost')
    depends_on('cmake', type='build')

    def install(self, spec, prefix):
        options = []
        options.extend(std_cmake_args)

        build_directory = join_path(self.stage.path, 'spack-build')
        source_directory = self.stage.source_path

        if '+debug' in spec:
            options.append('-DCMAKE_BUILD_TYPE:STRING=Debug')
        else:
            options.append('-DCMAKE_BUILD_TYPE:STRING=Release')

        with working_dir(build_directory, create=True):
            cmake(source_directory, *options)
            make()
            make("install")
