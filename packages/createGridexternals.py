#!/usr/bin/env python

import os, hashlib, urllib2, optparse

def get_remote_md5_sum(url, max_file_size=100*1024*1024):
    remote = urllib2.urlopen(url)
    hash = hashlib.md5()

    total_read = 0
    while True:
        data = remote.read(4096)
        total_read += 4096

        if not data or total_read > max_file_size:
            break

        hash.update(data)

    return hash.hexdigest()

template = """##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform
import re


class {0}(Package):
    \"\"\"{0} binary package, Grid externals\"\"\"

    homepage = "????"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "{1}-{3}-x86_64-slc6.tar.gz"

    v="{3}"
    plt = platform.system()

    if 'SLC6' in plt and '6.8' in plt:
        version(v+'-x86_64-slc6', '{4}')
    elif 'CentOS' in plt:
        version(v+'-x86_64-centos7', '{5}')
    else:
        version(v+'-x86_64-slc5', '{6}')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '%s' % source_directory,
            '%s' % prefix)
        env['{2}_HOME'] = prefix

    def url_for_version(self, version):
        return "%s{1}-%s.tar.gz" % (self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        spack_env.set('{2}_HOME', self.prefix)
"""

gridList = {
   #"cream":           "1.14.0-4",
   #epel":            "20130408",
   #"dpm":             "1.8.5-1",
   #"FTS":             "2.2.8emi2",
    #"FTS3":           "0.0.1-88",
   #"gfal2":           "2.2.0-1",
   #"gridftp_ifce":    "2.3.1-0",
   #"gridsite":        "1.7.25-1.emi2",
   "lb":              "3.2.9",
   "lcgdmcommon":     "1.8.5-1",
   #"lcginfosites":    "3.1.0-3",
   "lfc":             "1.8.5-1",
   "WMS":             "3.4.0",
}

for g in gridList:
    print "Creating package: ", g
    name = g
    version = gridList[g]

    n = name.replace("_","-")

    urlPackages =  "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/{0}-{1}-x86_64-{2}.tar.gz"
    # Checksums
    print "Looking for md5 in url: ", urlPackages.format(n, version, "slc6")
    md5_slc6 = get_remote_md5_sum(urlPackages.format(n, version, "slc6"))
    if "FTS3" not in name:
        md5_centos7 = get_remote_md5_sum(urlPackages.format(n, version, "centos7"))
        md5_slc5 = get_remote_md5_sum(urlPackages.format(n, version, "slc5"))

    os.mkdir(name.lower())
    with open(name.lower()+'/package.py','w') as f:
        f.write(template.format(
            name.title(),
            n,
            name.upper(),
            version,
            md5_slc6,
            md5_centos7,
            md5_slc5
        ))
