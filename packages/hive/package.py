##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Hive(Package):
    """The Apache Hive data warehouse software facilitates reading, writing,
    andmanaging large datasets residing in distributed storage using SQL"""

    homepage = "https://hive.apache.org"
    list_url = "http://mirror.easyname.ch/apache/hive"

    version('2.1.1', '782cd36d0dc710ffbf0ece41081b2b85')
    version('2.0.1', '67fde675d81ab3d9fb9962e22d17fa40')
    version('1.2.1', '3f51e327599206f1965e105de7be68eb')
    version('1.1.1', 'cdfe203c110b8fc3120af632e768f011')

    depends_on('cmake')
    depends_on('jdk@1.7:')
    depends_on('hadoop@2.0:', when='@2.0.0:')
    depends_on('hadoop@1.0:', when='@:1.9.999')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '{0}'.format(source_directory),
            '{0}'.format(prefix))

    def url_for_version(self, version):
        return "{0}/hive-{1}/apache-hive-{1}-bin.tar.gz".format(self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        spack_env.set('HIVE_HOME', self.prefix)
        spack_env.prepend_path('PATH', join_path(self.prefix, 'bin'))
