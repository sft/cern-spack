##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Openldap(AutotoolsPackage):
    """OpenLDAP Software is an open source implementation of the Lightweight
    Directory Access Protocol."""

    homepage = "http://www.openldap.org/"
    url      = "https://www.openldap.org/software/download/OpenLDAP/openldap-release/openldap-2.4.9.tgz"

    version('2.4.44', '693ac26de86231f8dcae2b4e9d768e51')
    version('2.4.43', '49ca65e27891fcf977d78c10f073c705')
    version('2.4.42', '47c8e2f283647a6105b8b0325257e922')
    version('2.4.41', '3f1a4cea52827e18feaedfdc1634b5d0')
    version('2.4.40', '423c1f23d2a0cb96b3e9baf7e9d7dda7')
    version('2.4.39', 'b0d5ee4b252c841dec6b332d679cf943')

    depends_on('db')
