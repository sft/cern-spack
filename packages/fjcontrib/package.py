##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Fjcontrib(AutotoolsPackage):
    """The fastjet-contrib space is intended to provide a common location for
    access to 3rd party extensions of FastJet."""

    homepage = "https://fastjet.hepforge.org/contrib/"
    url      = "http://fastjet.hepforge.org/contrib/downloads/fjcontrib-1.026.tar.gz"

    version('1.026', 'cdef3a0e3a510b89d88bda4acbe5b199')
    version('1.025', 'a25cc392d75663d9059247c615830783')
    version('1.024', 'eba00054765807215a3d4429773fb7e1')
    version('1.023', 'c237e4ace4b1e236d4e16fef54781096')
    version('1.022', '28a7247b6df196513dda95d9b94b751d')

    depends_on('fastjet')

    def configure_args(self):
        spec = self.spec
        args = [
             '--fastjet-config=%s' %
             join_path(spec['fastjet'].prefix, 'bin/fastjet-config')
        ]
        return args
