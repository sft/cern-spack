##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyMetakernel(PythonPackage):
    """A Jupyter/IPython kernel template which includes core magic functions
    (including help, command and file path completion, parallel and distributed
    processing, downloads, and much more)."""

    homepage = "https://github.com/Calysto/metakernel"
    url      = "https://pypi.io/packages/source/m/metakernel/metakernel-0.20.1.tar.gz"

    version('0.20.1', '296f1885933fc7b3b2622ed31b79f5bf')

    depends_on('py-setuptools', type='build')
    depends_on('py-ipykernel', type=('build', 'run'))
    depends_on('py-pexpect@4.2:', type=('build', 'run'))
