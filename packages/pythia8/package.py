##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Pythia8(AutotoolsPackage):
    """FIXME: Put a proper description of your package here."""

    homepage = "http://www.example.com"
    url      = "http://home.thep.lu.se/~torbjorn/pythia8/pythia8223.tgz"

    version('8223', 'ccb52b9172ffcdc426732a95f7bf4dbb')
    version('8219', '3459b52b5da1deae52cbddefa6196feb')
    version('8215', 'b4653133e6ab1782a5a4aa66eda6a54b')
    version('8212', '0886d1b2827d8f0cd2ae69b925045f40')

    depends_on('boost')
    depends_on('hepmc')

    def configure_args(self):
        args = [
            '--with-boost=%s' % self.spec['boost'].prefix,
            '--with-boost-include=%s' %
                join_path(self.spec['boost'].prefix,'include')
        ]
        return args
