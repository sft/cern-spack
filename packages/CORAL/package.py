##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform

class Coral(Package):
    """"""

    homepage = "https://twiki.cern.ch/twiki/bin/view/Persistency/Coral"
    url = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CORAL-3_1-patches.tar.gz'

    version('HEAD', svn='svn.cern.ch/reps/lcgcoral/coral/trunk')
    version('3_1-patches', '4262e6ac22b5bbf5c3090faf3598ddaf')
    version('3_1_7', '458134a2115d6a3bd3d322993a9d26f2')
    version('3_1_6', '40d65e696c89a67c42e5f58dcc14cc83')

    depends_on('cmake', type='build')

    depends_on("ninja")
    depends_on("ccache")
    depends_on("boost")
    depends_on("cppunit")
    depends_on("expat")
    depends_on("frontier_client") # add package
    depends_on("libaio")
    depends_on("mysql")
    depends_on("oracle")
    depends_on("python")
    #depends_on("qmtest") # necessary??
    depends_on("xerces-c")
    depends_on("gperftools")

    def install(self, spec, prefix):

        build_directory = join_path(self.stage.path, 'spack-build')
        source_directory = self.stage.source_path

        c = str(self.spec.compiler)
        c = c.split('@')
        comp = "{0}{1}".format(c[0], "".join(c[1].split('.')[0:2]))
        plt = "{0}-{1}-{2}".format(platform.machine(), platform.system().lower(), comp)

        cmake_args = [source_directory]

        if '+debug' in spec:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Debug')
            plt += '-dbg'
        else:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Release')
            plt += '-opt'

        cmake_args.extend([
            '-DCMAKE_INSTALL_PREFIX=%s' % prefix,
            '-DCMAKE_VERBOSE_MAKEFILE=ON',
            '-DBINARY_TAG=%s' % plt
        ])

        if 'cxx11' in spec:
            cmake_args.append('-DCMAKE_CXX_COMPILER=-std=c++11')
        if 'cxx14' in spec:
            cmake_args.append('-DCMAKE_CXX_COMPILER=-std=c++14')
        if 'cxx1y' in spec:
            cmake_args.append('-DCMAKE_CXX_COMPILER=-std=c++1y')

        if '+python@3' in spec:
            cmake_args.append('-Dpython3=ON')

        cmake_args.extend(std_cmake_args)

        with working_dir(build_directory, create=True):
            cmake(*cmake_args)
            make()
            make("install")

    def url_for_version(self, version):
        return "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/CORAL-%s.tar.gz" % version
