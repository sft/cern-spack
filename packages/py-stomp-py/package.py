##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyStompPy(PythonPackage):
    """stomp.py is a Python client library for accessing messaging servers
    (such as Apollo or RabbitMQ) using the STOMP protocol (versions 1.0, 1.1
    and 1.2). It can also be run as a standalone, command-line client for
    testing. """

    homepage = "https://github.com/jasonrbriggs/stomp.py"
    url      = "https://pypi.io/packages/source/s/stomp.py/stomp.py-4.1.17.tar.gz"

    version('4.1.17', '5a8d60d2b933b599f32d0f0404107944')
    version('4.1.16', 'f222180992c0c4a4de92fc3e08a098f8')
    version('4.1.15', '1b70d16572e0527cbabe8b1958dcf00f')
    version('4.1.13', '21879706b4f8ebde7d634183be3104c1')
    version('4.1.11', 'ffd708d52c34f9d64d2920b96e352a27')

    depends_on('py-setuptools', type='build')
