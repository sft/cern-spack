##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Alpgen(CMakePackage):
    """A collection of codes for the generation of multi-parton processes in
    hadronic collisions."""

    homepage = "http://mlm.home.cern.ch/mlm/alpgen/"
    url      = "http://mlm.home.cern.ch/mlm/alpgen/V2.1/v213.tgz"

    version('2.13', 'b0a9b2045ea7439b9e8c9cca64c057e1')
    version('2.12', '7b7724aec224187f45f293eb52e11fdb')

    variant('debug', default=False, description='Build debug version')

    def build_type(self):
        spec = self.spec
        if '+debug' in spec:
            return 'Debug'
        else:
            return 'Release'

    def url_for_version(self, version):
        v = str(version).replace(".", "")
        url = "http://mlm.home.cern.ch/mlm/alpgen/V2.1/v{0}.tgz".format(v)
        return url
