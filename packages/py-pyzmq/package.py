##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyPyzmq(PythonPackage):
    """PyZMQ: Python bindings for zeromq."""

    homepage = "https://github.com/zeromq/pyzmq/"
    url      = "https://github.com/zeromq/pyzmq/archive/v16.0.2.tar.gz"

    version('16.0.2', '4cf14a2995742253b2b009541f4436f4')
    version('16.0.1', '8c59659a347c10ad349e6d8cfa45be03')
    version('16.0.0', 'a62b290299b7ee1627614358e1f5562d')
    version('15.4.0', '404edb66fcd4a6e3a1002bb427ef9ca5')
    version('15.3.0', '58ae1343774900f49607fd8f469b37a7')
    version('15.2.0', '8dbe6b53c4cfad1c36c22f72c0bb47da')
    version('15.1.0', 'eb3b5a70f9ff623c651de3c59b3cad96')
    version('15.0.0', '9167ff143f9ad6f8392d6f9c9584bd09')
    version('14.7.0', 'bb4de52b715923f779d3233446329df0')
    version('14.6.0', 'a82ce31a5beb01ab17a017cb39cf07af')
    version('14.5.0', 'd69df0a51a13ba42788351eefe78e7ff')

    depends_on('py-setuptools', type='build')
    depends_on('py-cython', type=('build', 'run'))
    depends_on('zeromq', type=('build', 'run'))
