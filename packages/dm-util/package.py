##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *
import platform

class DmUtil(Package):
    """Nothing"""

    #homepage = "???"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"

    plt = platform.platform()

    # SLC5
    if 'redhat-5' in plt:
        version('1.15.0-0' , '93c5996e447e4b996fe9bddf17d65543',
        url = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/dm-util-1.15.0-0-x86_64-slc5.tar.gz')
    else: #SLC6, CC7, centos7
        version('1.15.0-0', 'fffa9497c93d078a13ce2a79fb1f9c9c',
        url = 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/dm-util-1.15.0-0-x86_64-cc7.tar.gz')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '{0}'.format(source_directory),
            '{0}'.format(prefix))
