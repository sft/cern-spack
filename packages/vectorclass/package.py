##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class Vectorclass(Package):
    """This is a collection of C++ classes, functions and operators that makes
    it easier to use the the vector instructions (Single Instruction Multiple
    Data instructions) of modern CPUs without using assembly language"""

    homepage = "http://www.agner.org/optimize/#vectorclass"
    url      = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/vectorclass-1.16.tar.gz"

    depends_on('cmake')

    version('1.16', 'b6dbb33db1f44af4998b74a1c8ee508c')

    def install(self, spec, prefix):
        cmake('-E', 'copy_directory', '{0}'.format(self.stage.source_path),
            '{0}'.format(prefix))
