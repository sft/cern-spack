##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *
import platform
import re


def getPlatform():
    arch = platform.machine()

    system = platform.system()

    #---Determine the OS and version--------------------------------------
    if system == 'Darwin' :
       osvers = 'mac' + string.join(platform.mac_ver()[0].split('.')[:2], '')
    elif system == 'Linux' :
       dist = platform.linux_distribution()
       if re.search('SLC', dist[0]):
          osvers = 'slc' + dist[1].split('.')[0]
       elif re.search('CentOS', dist[0]):
          osvers = 'centos' + dist[1].split('.')[0]
       elif re.search('Ubuntu', dist[0]):
          osvers = 'ubuntu' + dist[1].split('.')[0]
       elif re.search('Fedora', dist[0]):
          osvers = 'fedora' + dist[1].split('.')[0]
       else:
          distribution = platform.linux_distribution()
          osvers = 'linux' + string.join(distribution[1].split('.')[:2], '')
    elif system == 'Windows':
       osvers = win + platform.win32_ver()[0]
    else:
       osvers = 'unk-os'
    return '%s-%s' %(arch, osvers)

plt = getPlatform()

class Voms(Package):
    """<Pending>"""

    homepage = "????"
    list_url = "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/"
    url      = list_url + "voms-2.0.9-1-x86_64-slc6.tar.gz"

    v="2.0.9-1"

    if 'slc6' in plt:
        version('2.0.9-1-x86_64-slc6', '7065f14150fa6a0a2f42c07fef9cf5bd')
    elif 'slc5' in plt:
        version(v+'-x86_64-slc5', '5f54e01dfc640e80dd514bfc8b83987d')
    elif 'centos7':
        version(v+'-x86_64-centos7', '7065f14150fa6a0a2f42c07fef9cf5bd')
    else:
        version(v+'-x86_64-cc7', '7065f14150fa6a0a2f42c07fef9cf5bd')

    depends_on("cmake", type='build')

    def install(self, spec, prefix):
        source_directory = self.stage.source_path

        cmake('-E', 'copy_directory', '{0}'.format(source_directory),
            '{0}'.format(prefix))
        env['VOMS_HOME'] = prefix

    def url_for_version(self, version):
        return "{0}voms-{1}.tar.gz".format(self.list_url, version)

    def setup_dependent_environment(self, spack_env, run_env, extension_spec):
        spack_env.set('VOMS_HOME', self.prefix)
