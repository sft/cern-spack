##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *

class Relax(Package):
    """The RELAX projects provides a set of Reflex dictionaries for commonly
    used libraries in LCG."""

    homepage = "https://twiki.cern.ch/twiki/bin/view/LCG/RELAX"

    version('RELAX-root6', svn='http://svn.cern.ch/guest/relax/tags/RELAX-root6/relax/')

    depends_on('cmake@3.6:', type='build')

    depends_on("clhep")
    depends_on("cmaketools")
    depends_on("ROOT")
    depends_on("hepmc")
    depends_on("gsl")
    depends_on("heppdt")

    patch('RELAX-RELAX-root6.patch', when='@RELAX-root6')

    def install(self, spec, prefix):
        build_directory = join_path(self.stage.path, 'spack-build')
        source_directory = self.stage.source_path

        cmake_args = [source_directory]

        if '+debug' in spec:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Debug')
        else:
            cmake_args.append('-DCMAKE_BUILD_TYPE:STRING=Release')

        cmaketools_modules = spec['cmaketools'].prefix + \
         '/share/CMakeTools/modules'

        cmake_args.extend(['-DCMAKE_INSTALL_PREFIX=%s' % prefix,
                       '-DROOTSYS=%s' % spec['ROOT'].prefix,
                       '-DCMAKE_MODULE_PATH=%s' % cmaketools_modules,
                       '-DCLHEP_ROOT_DIR=%s' % spec['clhep'].prefix,
                       '-DHEPMC_ROOT_DIR=%s' % spec['hepmc'].prefix,
                       '-DGSL_ROOT_DIR=%s' % spec['gsl'].prefix,
                       '-DHEPPDT_ROOT_DIR=%s' % spec['heppdt'].prefix])

        if 'cxx11' in spec:
           cmake_args.append('-DCMAKE_CXX_FLAGS=-std=c++11')
        elif 'cxx14' in spec:
           cmake_args.append('-DCMAKE_CXX_FLAGS=-std=c++14')
        else : #'cxx1y'
           cmake_args.append('-DCMAKE_CXX_FLAGS=-std=c++1y')

        cmake_args.extend(std_cmake_args)

        with working_dir(build_directory, create=True):
            cmake(*cmake_args)
            root_home = spec['ROOT'].prefix,
            #make("ROOTSYS=%s" % root_home)
            make("install")
