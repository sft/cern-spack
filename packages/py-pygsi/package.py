##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the LICENSE file for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *


class PyPygsi(PythonPackage):
    """GSI aware openssl wrapper for python """

    # REVIEW: github repo call it pyGSI but just GSI in pypi.org
    homepage = "https://github.com/acasajus/pyGSI"
    url      = "https://pypi.io/packages/source/G/GSI/GSI-0.6.4.tar.gz"

    version('0.6.4', '184f54dbd5aa7986c10e749c80c18feb')
    # version('0.6.3', '450e55a9a07f0f624b653c1ef7fa7151') not in pypi.org
    version('0.6.2', '75be10bf4cf7998ddd5c47b66b7969ca')
    # version('0.6.1', 'da6f95087faee19edfa9500f7381dd27') not in pypi.org

    depends_on('py-setuptools', type='build')
